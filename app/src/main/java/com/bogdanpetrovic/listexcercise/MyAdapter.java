package com.bogdanpetrovic.listexcercise;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by Petrovic on 20.7.2016..
 */
public class MyAdapter extends ArrayAdapter<String> {

    public MainActivity mainActivity;
    private final Context context;
    private final String[] values;

    public MyAdapter(Context context, String[] values) {
        super(context, R.layout.items, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = layoutInflater.inflate(R.layout.items, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.textViewTitle);
        title.setText(values[position]);


        return rowView;
    }
}
